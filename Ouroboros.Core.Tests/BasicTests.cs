using Ouroboros.Core;

namespace Ouroboros.Core.Tests;

public class BasicTests
{
    [SetUp]
    public void Setup()
    {
    }

    [Test]
    public void HelloWorldTest()
    {
        HelloWorld.Main();
        Console.WriteLine($"Project = {typeof(HelloWorld).Assembly.GetName().FullName}");
    }
}
