using Nuke.Common;
using Nuke.Common.CI.GitLab;
using Nuke.Common.Execution;
using Nuke.Common.IO;
using Nuke.Common.ProjectModel;
using Nuke.Common.Tooling;
using Nuke.Common.Tools.DotNet;
using Nuke.Common.Tools.GitVersion;
using Nuke.Common.Tools.NuGet;
using Nuke.Common.Utilities;
using Ouroboros.Core;
using System.Collections.Generic;
using Serilog;
using System.Linq;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Immutable;
using Nuke.Common.Tools.Docker;
using Nuke.Common.Tools.Git;

/// <summary>
/// Stages implementation :
/// 
/// - Install(local)
/// 
/// - Info, 
/// - Clean,
/// - Restore,
/// - Build, 
/// - UnitTest(develop, release), 
/// 
/// - LocalPack(local), 
///
/// - Release[tag], 
/// - Deploy[Pack, Deploy Gitlab, Docker](main), 
/// - Monitoring[test url package, test url docker], 
/// - Announce[discord](main, public)
/// 
/// </summary>
[UnsetVisualStudioEnvironmentVariables]
public class Build : NukeBuild, INukeBuild, IGitTarget, IInstall, IClean, IRestore, ICompile, IUnitTests, IDeploy, IMonitoring, IAnnounce
{
    public Build()
    {
        DockerTasks.DockerLogger = (type, text) => Log.Debug(text); 
        GitTasks.GitLogger = (type, text) => Log.Debug(text);
    }

    public static int Main() => Execute<Build>(x => ((IInfo)x).Info, x => ((ICompile)x).Compile);

    [Parameter("Configuration to build - Default is 'Debug' (local) or 'Release' (server)")]
    public Configuration Configuration => IsLocalBuild ? Configuration.Debug : Configuration.Release;

    [Solution]
    public Solution Solution;

    [Parameter("Main project name")]
    public string Project;

    [Parameter]
    public long GitlabProjectId
    { get => IsLocalBuild ? _GitlabProjectId : GitLab.Instance.ProjectId; set => _GitlabProjectId = value; }
    long _GitlabProjectId;

    [Parameter]
    public string GitlabToken;

    [Parameter]
    public string OuroborosPackageId;

    [Parameter("Local source directory for temporary nuget packages")]
    public string NugetLocalDirTemporary = "C:\\.nuget\\packages";

    [GitVersion]
    public GitVersion GitVersion;

    [PathVariable]
    public Tool Curl;

    [Parameter]
    public int NumberCIPipelineToRemove = 50; // 100 max

    public AbsolutePath SourceDirectory => RootDirectory / $"{Project}";
    public AbsolutePath TestDirectory => RootDirectory / $"{Project}.Tests";
    public AbsolutePath OutputDirectory => SourceDirectory / "bin" / Configuration;

    Target IInstall.Install => _ => _
        .OnlyWhenStatic(() => IsLocalBuild)
        .Inherit<IInstall>()
        .Executes(() =>
        {
            DotNetTasks.DotNetNuGetAddSource(_ => _
                .SetSource(NugetLocalDirTemporary)
                .SetName("LocalPackages")
            );
        });

    Target IInfo.Info => _ => _
        .Inherit<IInfo>(x => x.Info)
        .Executes(() =>
        {
            Log.Information("Solution path = {Value}", Solution);
            Log.Information("Solution directory = {Value}", Solution.Directory);
            Log.Information("Project = {Value}", Project);
            if (IsLocalBuild)
            {
                Log.Information("NugetLocalDirTemporary = {Value}", NugetLocalDirTemporary);
            }

            var r = Curl($"-k -L -sb -H \"Accept: application/json\" https://gitlab.com/api/v4/projects/{GitlabProjectId}/packages?order_by=version", logOutput: false);
            if (r != null)
            {
                JArray data = JArray.Parse(r.First().Text);
                Log.Information("Name = {Value}", data[0].Value<string>("name"));
                Log.Information("Version = {Value}", data[0].Value<string>("version"));
            }
            else
            {
                Log.Error("No package on Gitlab");
            }

            if(ToolResolver.TryGetEnvironmentTool("docker") != null)
            {
                DockerTasks.DockerInfo();
            }
            else
            {
                Log.Warning("No docker installed.");
            }
        });

    Target IClean.Clean => _ => _
        .Inherit<IClean>()
        .Executes(() =>
        {
            DotNetTasks.DotNetClean(_ => _.SetProject(Solution));
            SourceDirectory.GlobDirectories("**/bin", "**/obj").DeleteDirectories();
            TestDirectory.GlobDirectories("**/bin", "**/obj").DeleteDirectories();
        });

    Target IRestore.Restore => _ => _
        .Inherit<IRestore>()
        .Executes(() =>
        {
            DotNetTasks.DotNetRestore(s => s.SetProjectFile(Solution));
        });

    Target ICompile.Compile => _ => _
        .Inherit<ICompile>()
        .Executes(() =>
        {
            DotNetTasks.DotNetBuild(_ => _
                .SetProjectFile(Solution)
                .SetConfiguration(Configuration)
                .SetVersion(GitVersion.NuGetVersionV2)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .SetFileVersion(GitVersion.AssemblySemFileVer)
                .EnableNoCache());
        });

    Target IUnitTests.UnitTests => _ => _
        .Inherit<IUnitTests>()
        .Executes(() =>
        {
            DotNetTasks.DotNet($"list {Project}.Tests package");
            DotNetTasks.DotNet($"list {Project}.Tests reference");
            DotNetTasks.DotNetTest(_ => _.SetProjectFile(Solution));
        });

    public Target LocalPack => _ => _
        .OnlyWhenStatic(() => IsLocalBuild)
        .DependsOn<IUnitTests>()
        .Executes(() =>
    {
        DotNetTasks.DotNetPack(_ => _
               .SetProject(Solution)
               .SetConfiguration(Configuration)
               .SetOutputDirectory(OutputDirectory)
               .EnableNoBuild()
               .EnableNoRestore()
               .SetVersion(GitVersion.NuGetVersionV2)
               .SetAssemblyVersion(GitVersion.AssemblySemVer)
               .SetInformationalVersion(GitVersion.InformationalVersion)
               .SetFileVersion(GitVersion.AssemblySemFileVer));

        RelativePath packageFile = RootDirectory.GetRelativePathTo(OutputDirectory / $"{Project}.{GitVersion.NuGetVersionV2}.nupkg");
        NuGetTasks.NuGet($"add {packageFile} -Source {NugetLocalDirTemporary}");
    });

    Target IDeploy.Deploy => _ => _
        .Requires(() => Host is GitLab)
        .Inherit<IDeploy>()
        .Triggers(DeployDocker)
        .Executes(() =>
        {
            DotNetTasks.DotNetPack(_ => _
                .SetProject(Solution)
                .SetConfiguration(Configuration)
                .SetOutputDirectory(OutputDirectory)
                .EnableNoBuild()
                .EnableNoRestore()
                .SetVersion(GitVersion.NuGetVersionV2)
                .SetAssemblyVersion(GitVersion.AssemblySemVer)
                .SetInformationalVersion(GitVersion.InformationalVersion)
                .SetFileVersion(GitVersion.AssemblySemFileVer));

            DotNetTasks.DotNetNuGetPush(_ => _
                .SetSource($"https://gitlab.com/api/v4/projects/{GitLab.Instance.ProjectId}/packages/nuget/index.json")
                .SetTargetPath(OutputDirectory / $"{Project}.{GitVersion.NuGetVersionV2}.nupkg")
                .SetApiKey(GitLab.Instance.JobToken)
                );
        });

    public Target DeployDocker => _ => _
        .OnlyWhenStatic(() => Host is GitLab /*&& !((IReleaseDependency)this).IsReleaseSkipped*/)
        .DependsOn<IDeploy>()
        .TryDependentFor<IMonitoring>()
        .Executes(() =>
        {
            DockerTasks.DockerLogin(_ => _
               .SetUsername(GitLab.Instance.RegistryUser)
               .SetPassword(GitLab.Instance.RegistryPassword)
               .SetServer(GitLab.Instance.Registry)
            );

            DockerTasks.DockerBuild(_ => _
                .SetPull(true)
                .SetPath(".")
                .SetTag(new string[] {
                    $"{GitLab.Instance.RegistryImage}:latest",
                    $"{GitLab.Instance.RegistryImage}:{GitVersion.NuGetVersionV2}"
                })
                .SetBuildArg(new string[] {
                    $"REPO_PATH={GitLab.Instance.ProjectDirectory}"
                })
            );

            DockerTasks.DockerPush(_ => _
                .SetName($"{GitLab.Instance.RegistryImage}")
                .SetAllTags(true)
            );

            DockerTasks.DockerLogout(_ => _
                .SetServer(GitLab.Instance.Registry));
        });

    Target IMonitoring.Monitoring => _ => _
        .OnlyWhenStatic(() => Host is GitLab)
        .Inherit<IMonitoring>()
        .Executes(() =>
        {
            Log.Information("Check package :");
            var r = Curl($"-k -L -H \"Accept: application/json\" https://gitlab.com/api/v4/projects/{GitLab.Instance.ProjectId}/packages/{OuroborosPackageId}", logOutput: false);
            if (r != null)
            {
                Log.Information("Name = {Value}", r.StdToJson().Value<string>("name"));
                Log.Information("Version = {Value}", r.StdToJson().Value<string>("version"));
            }
            else
            {
                Log.Error("No package on Gitlab");
            }

            Log.Information("Check docker :");
            DockerTasks.DockerLogin(_ => _
                .SetUsername(GitLab.Instance.RegistryUser)
                .SetPassword(GitLab.Instance.RegistryPassword)
                .SetServer(GitLab.Instance.Registry)
            );

            DockerTasks.DockerPull(_ => _
                .SetName($"{GitLab.Instance.RegistryImage}:latest")
                .SetProcessExitHandler(p =>
                {
                    switch (p.ExitCode)
                    {
                        case -1:
                            throw new Exception("Invalid args");
                        case > 0:
                            throw new Exception($"{p.ExitCode}, tests have failed");
                    }
                })
            );
        });


    // GitLab.Instance.JobToken not enough rights for CI
    Target CleanCI => _ => _
       .OnlyWhenStatic(() => IsLocalBuild)
       .Requires(() => GitlabToken)
       .Executes(() =>
       {
           var r = Curl($"-k -L -sb -H \"Accept: application/json\" https://gitlab.com/api/v4/projects/{GitlabProjectId}/pipelines?per_page={NumberCIPipelineToRemove}&sort=asc", logOutput: false);

           if (r != null)
           {
               JArray data = JArray.Parse(r.First().Text);
               var pipelineIds = data.Children().Select(x => x.Value<string>("id"));
               Log.Information("{Value}", pipelineIds);

               if (IsLocalBuild)
               {
                   foreach (string pipeline in pipelineIds)
                   {
                       Curl($"-k -L --header \"PRIVATE-TOKEN:{GitlabToken}\" --request \"DELETE\" https://gitlab.com/api/v4/projects/{GitlabProjectId}/pipelines/{pipeline}", exitHandler: p =>
                       {
                           switch (p.ExitCode)
                           {
                               case -1:
                                   throw new Exception("Invalid args");
                               case > 0:
                                   throw new Exception($"{p.ExitCode}, tests have failed");
                           }
                       });
                       Log.Information("Delete pipeline {Value}", pipeline);
                   }
               }
           }
       });
}
