using Nuke.Common;
using Nuke.Common.Tools.DotNet;
using Serilog;

namespace Ouroboros.Core;

public interface IOuroborosInstall : INukeBuild, IInstall
{
    [Parameter]
    public string OuroborosProjectID => TryGetValue(() => OuroborosProjectID);

    //https://docs.gitlab.com/ee/user/packages/nuget_repository/
    public new Target Install => _ => _
        .Inherit<IInstall>()
        .Executes(() =>
        {
            Log.Information("Ouroboros ProjectID = {Value}", OuroborosProjectID);

            string source = $"https://gitlab.example.com/api/v4/projects/{OuroborosProjectID}/packages/nuget/index.json";
            Log.Information("Try install source ", source);

            //dotnet nuget add source "https://gitlab.example.com/api/v4/projects/<your_project_id>/packages/nuget/index.json" --name < source_name > --username < gitlab_username or deploy_token_username> --password < gitlab_personal_access_token or deploy_token>
            // Without authentification need a public repository
            DotNetTasks.DotNetNuGetAddSource(_ => _
                    .SetSource(source)
                    .SetName("Ouroboros")
                );

            //DotNetTasks.DotNet($"add {Project}.Tests package {Project} -v {GitVersion.NuGetVersionV2} -s {NugetLocalTemp}");
        });
}
