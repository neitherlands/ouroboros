﻿using Nuke.Common;
using Nuke.Common.Utilities.Collections;
using Serilog;

/// <summary>
/// Based on GitFlow.
/// Stages implementation :
/// 
/// - Install(once)
/// 
/// - Info, 
/// - Clean,
/// - Restore,
/// - Build, 
/// - UnitTest(develop), 
/// 
/// - Coverage, 
/// - IntegrationTests, 
/// - PreDeploy, 
/// - EndToEndTests[fonctionnal, performances](release, hotFix), 
/// 
/// - Release, 
/// - Deploy, 
/// - Monitoring, 
/// - Announce(main, public)
/// 
/// </summary>
namespace Ouroboros.Core
{
    public interface IInstall : INukeBuild
    {
        public Target Install => _ => _
            .TryBefore<IInfo>()
            .Executes(() => { });
    }

    public interface IInfo : INukeBuild
    {
        public Target Info => _ => _
            .Executes(() => { });
    }

    public interface IClean : INukeBuild
    {
        public Target Clean => _ => _
            .TryAfter<IInfo>()
            .Executes(() => { });
    }

    public interface IRestore : INukeBuild
    {
        public Target Restore => _ => _
            .TryDependsOn<IClean>()
            .Executes(() => { });
    }
    
    public interface ICompile : INukeBuild
    {
        public Target Compile => _ => _
            .TryDependsOn<IClean>()
            .TryDependsOn<IRestore>()
            .TryTriggers<IUnitTests>()
            .Executes(() => { });
    }

    public interface IUnitTests : INukeBuild
    {
        public Target UnitTests => _ => _
            .TryDependsOn<ICompile>()
            .Executes(() => { });
    }

    public interface ICoverage : INukeBuild
    {
        public Target Coverage => _ => _
            .TryDependsOn<IUnitTests>()
            .TryTriggeredBy<IUnitTests>()
            .Executes(() => { });
    }

    public interface IIntegrationTests : INukeBuild
    {
        public Target IntegrationTests => _ => _
            .TryDependsOn<IUnitTests>()
            .Executes(() => { });
    }

    public interface IPredeploy : INukeBuild
    {
        public Target Predeploy => _ => _
            .TryDependsOn<IIntegrationTests>()
            .Executes(() => { });
    }

    public interface IE2ETests : INukeBuild
    {
        public Target E2ETests => _ => _
            .TryDependsOn<IPredeploy>()
            .Executes(() => { });
    }

    public interface IReleaseDependency : INukeBuild
    {
        public bool IsReleaseSkipped => SkippedTargets.Any(x => x.Name == "Release");
    }

    public interface IRelease : INukeBuild, IReleaseDependency
    {
        public Target Release => _ => _
            .WhenSkipped(DependencyBehavior.Skip)
            .TryDependsOn<IUnitTests>()
            .TryDependsOn<IIntegrationTests>()
            .TryDependsOn<IPredeploy>()
            .TryDependsOn<IE2ETests>()
            //.TryTriggers<IDeploy>() // Not working on CI
            .Executes(() => { });
    }

    public interface IDeploy : INukeBuild, IReleaseDependency
    {
        public Target Deploy => _ => _
            //.OnlyWhenStatic(() => IsServerBuild && !IsReleaseSkipped)
            .WhenSkipped(DependencyBehavior.Skip)
            .TryDependsOn<IRelease>()
            .TryTriggers<IMonitoring>()
            .TryTriggers<IAnnounce>()
            .Executes(() => { });
    }

    public interface IMonitoring : INukeBuild, IReleaseDependency
    {
        public Target Monitoring => _ => _
            //.OnlyWhenStatic(() => IsServerBuild && !IsReleaseSkipped)
            .TryDependsOn<IDeploy>()
            .Executes(() => { });
    }

    public interface IAnnounce : INukeBuild, IReleaseDependency
    {
        public Target Annonce => _ => _
            //.OnlyWhenStatic(() => IsServerBuild)
            .TryAfter<IMonitoring>()
            .Executes(() => { });
    }
}
