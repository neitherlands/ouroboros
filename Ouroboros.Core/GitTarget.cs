﻿using Nuke.Common;
using Nuke.Common.CI.GitLab;
using Nuke.Common.Git;
using Nuke.Common.Tools.Git;
using Nuke.Common.Tools.GitVersion;
using Serilog;

namespace Ouroboros.Core;

public interface IGitTarget : INukeBuild, IInfo, IRelease
{
    [GitVersion]
    public GitVersion GitVersion => TryGetValue(() => GitVersion);

    [GitRepository]
    public GitRepository GitRepository => TryGetValue(() => GitRepository);

    public Target GitInfo => _ => _
        .DependentFor<IInfo>()
        .TryAfter<IInstall>()
        .Executes(() =>
        {
            Log.Information("Git Version = {Value}", GitVersion);
            Log.Information("Version Project = {Value}", GitVersion.FullSemVer);
            Log.Information("Https URL = {Value}", GitRepository.HttpsUrl);
            Log.Information("SSH URL = {Value}", GitRepository.SshUrl);

            Log.Information("Commit = {Value}", GitRepository.Commit);
            Log.Information("Branch = {Value}", GitRepository.Branch);
            Log.Information("Tags = {Value}", GitRepository.Tags);

            Log.Information("main branch = {Value}", GitRepository.IsOnMainBranch());
            Log.Information("main/master branch = {Value}", GitRepository.IsOnMainOrMasterBranch());
            Log.Information("release/* branch = {Value}", GitRepository.IsOnReleaseBranch());
            Log.Information("hotfix/* branch = {Value}", GitRepository.IsOnHotfixBranch());
        });

    Target IRelease.Release => _ => _
        .OnlyWhenStatic(() => GitRepository.IsOnMainOrMasterBranch())
        .Inherit<IRelease>()
        .Executes(() =>
        {
            if(IsLocalBuild)
            { 
                Log.Information("Make a release : {Value}", GitVersion.MajorMinorPatch);
                GitTasks.Git($"tag {GitVersion.MajorMinorPatch}");
                GitTasks.Git($"push origin --follow-tags");
            }
            else
            {
                Log.Information("Release trigered for {Value}", GitLab.Instance.CommitTag);
            }
        });
}
